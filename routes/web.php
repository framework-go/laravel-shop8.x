<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

//Route::get('/', [App\Http\Controllers\PagesController::class, 'home'])->name('home');

# 首页重定向
Route::redirect('/', '/products')->name('home');

Route::middleware(['auth', 'verified'])->group(
    function () {
        # 用户收货地址列表
        Route::get('user_addresses', [App\Http\Controllers\UserAddressesController::class, 'index'])
            ->name('user_addresses.index');
        # 创建收货地址表单
        Route::get('user_addresses/create', [App\Http\Controllers\UserAddressesController::class, 'create'])
            ->name('user_addresses.create');
        # 存储收货地址
        Route::post('user_addresses/store', [App\Http\Controllers\UserAddressesController::class, 'store'])
            ->name('user_addresses.store');
        # 编辑收货地址表单
        Route::get('user_addresses/{user_address}', [App\Http\Controllers\UserAddressesController::class, 'edit'])
            ->name('user_addresses.edit');
        # 更新收货地址
        Route::put('user_addresses/{user_address}', [App\Http\Controllers\UserAddressesController::class, 'update'])
            ->name('user_addresses.update');
        # 删除收货地址
        Route::delete('user_addresses/{user_address}', [App\Http\Controllers\UserAddressesController::class, 'destroy'])
            ->name('user_addresses.destroy');
        # 商品列表
        Route::get('products', [App\Http\Controllers\ProductsController::class, 'index'])
            ->name('products.index');
        # 收藏商品
        Route::post('products/{product}/favorite', [App\Http\Controllers\ProductsController::class, 'favor'])
            ->name('products.favor');
        # 取消收藏
        Route::delete('products/{product}/favorite', [App\Http\Controllers\ProductsController::class, 'disfavor'])
            ->name('products.disfavor');
        # 收藏列表
        Route::get('products/favorites', [App\Http\Controllers\ProductsController::class, 'favorites'])
            ->name('products.favorites');
        # 加入购物车
        Route::post('cart', [App\Http\Controllers\CartController::class, 'add'])
            ->name('cart.add');
        # 移除购物车商品
        Route::delete('cart/{sku}', [App\Http\Controllers\CartController::class, 'remove'])
            ->name('cart.remove');
        # 购物车列表
        Route::get('cart', [App\Http\Controllers\CartController::class, 'index'])
            ->name('cart.index');
    }
);

# 商品详情
Route::get('products/{product}', [App\Http\Controllers\ProductsController::class, 'show'])
    ->name('products.show');
Auth::routes(['verify' => true]);

