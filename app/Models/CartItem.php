<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property mixed amount
 */
class CartItem extends Model
{
    use HasFactory;

    public $timestamps = false;

    public $fillable = ['amount'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function productSku(): BelongsTo
    {
        return $this->belongsTo(ProductSku::class);
    }
}
