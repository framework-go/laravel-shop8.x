<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddCartRequest;
use App\Models\CartItem;
use App\Models\ProductSku;
use App\Models\User;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function add(AddCartRequest $request): array
    {
        /**
         * @var User $user
         */
        $user = $request->user();
        $skuId = $request->input('sku_id');
        $amount = $request->input('amount');
        if ($cart = $user->cartItems()->where('product_sku_id', $skuId)->first()) {
            $cart->update(['amount' => $amount]);
        } else {
            $cart = new CartItem();
            $cart->amount = $amount;
            $cart->user()->associate($user);
            $cart->productSku()->associate($skuId);
            $cart->save();
        }

        return [];
    }

    public function remove(ProductSku $sku, Request $request): array
    {
        $request->user()->cartItems()->where('product_sku_id', $sku->id)->delete();

        return [];
    }

    public function index(Request $request)
    {
        $cartItems = $request->user()->cartItems()->with(['productSku.product'])->get();

        return view('cart.index', compact('cartItems'));
    }
}
