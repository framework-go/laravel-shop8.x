<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\UserAddress;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\UserAddressRequest;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Cache;

class UserAddressesController extends Controller
{
    public function index(Request $request)
    {
        $addresses = Cache::remember('user_addresses_' . $request->user()->id, 5 * 3600, function () use ($request) {
            return $request->user()->addresses;
        });
        return view('user_addresses.index', compact('addresses'));
    }

    public function create()
    {
        return view('user_addresses.create_and_edit', ['address' => new UserAddress()]);
    }

    public function store(UserAddressRequest $request): RedirectResponse
    {
        $request->user()->addresses()->create(
            $request->only(
                [
                    'province',
                    'city',
                    'district',
                    'address',
                    'zip',
                    'contact_name',
                    'contact_phone',
                ]
            )
        );
        Cache::forget('user_addresses_' . $request->user()->id);
        return redirect()->route('user_addresses.index');
    }

    public function edit(UserAddress $userAddress)
    {
        return view('user_addresses.create_and_edit', ['address' => $userAddress]);
    }

    public function update(UserAddressRequest $request, UserAddress $userAddress): RedirectResponse
    {
        $userAddress->update(
            $request->only(
                [
                    'province',
                    'city',
                    'district',
                    'address',
                    'zip',
                    'contact_name',
                    'contact_phone',
                ]
            )
        );
        Cache::forget('user_addresses_' . $request->user()->id);
        return redirect()->route('user_addresses.index');
    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(Request $request, UserAddress $userAddress): JsonResponse
    {
        $this->authorize('own', $userAddress);

        $userAddress->delete();
        Cache::forget('user_addresses_' . $request->user()->id);
        return response()->json(['success' => true]);
    }
}
