<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Request;

class InternalException extends Exception
{
    /**
     * @var string
     */
    private $messageForUser;

    public function __construct(string $message = "", string $message_for_user = '系统内部错误', int $code = 0)
    {
        parent::__construct($message, $code);
        $this->messageForUser = $message_for_user;
    }

    public function render(Request $request)
    {
        if ($request->expectsJson()) {
            return response()->json(['msg' => $this->messageForUser], $this->code);
        }

        return view('pages.error', ['message' => $this->messageForUser]);
    }
}
