<?php

declare(strict_types=1);

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\Product;
use Encore\Admin\Controllers\AdminController;

class ProductsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '商品';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid(): Grid
    {
        $grid = new Grid(new Product());

        $grid->column('id', __('admin.id'))->sortable();
        $grid->column('title', '商品名称');
        $grid->column('on_sale', '已上架')->display(
            function($value) {
                return $value ? '是' : '否';
            }
        );
        $grid->column('price', '价格');
        $grid->column('rating', '评分');
        $grid->column('sold_count', '销量');
        $grid->column('review_count', '评论数');
        $grid->column('created_at', __('admin.create_at'));

        $grid->actions(
            function($actions) {
                $actions->disableView();
                $actions->disableDelete();
            }
        );

        $grid->tools(
            function($tools) {
                $tools->batch(
                    function($batch) {
                        $batch->disableDelete();
                    }
                );
            }
        );

        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form(): Form
    {
        $form = new Form(new Product());

        $form->text('title', '商品名称')->rules(['required']);
        $form->image('image', '封面图片')->rules(['required']);
        $form->quill('description', '商品描述')->rules(['required']);
        $form->radio('on_sale', '上架')->options([1 => '是', 0 => '否'])->default(1);
        $form->hasMany(
            'skus',
            'SKU列表',
            function(Form\NestedForm $form) {
                $form->text('title', 'SKU 名称')->rules(['required']);
                $form->text('description', 'SKU 描述')->rules(['required']);
                $form->text('price', '单价')->rules(['required']);
                $form->text('stock', '剩余库存')->rules(['required']);
            }
        );

        $form->saving(
            function(Form $form) {
                $form->model()->price = collect($form->input('skus'))
                    ->where(Form::REMOVE_FLAG_NAME, 0)
                    ->min('price') ?: 0;
            }
        );

        return $form;
    }
}
