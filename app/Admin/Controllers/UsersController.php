<?php

declare(strict_types=1);

namespace App\Admin\Controllers;

use App\Models\User;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Controllers\AdminController;

class UsersController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '用户';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User());

        $grid->column('id', __('admin.id'));
        $grid->column('name', __('admin.name'));
        $grid->column('email', __('admin.email'));
        $grid->column('email_verified_at', '已验证邮箱')->display(
            function($value) {
                return $value ? '是' : '否';
            }
        );
        $grid->column('created_at', __('admin.create_at'));
        $grid->column('updated_at', __('admin.update_at'));
        // 不在页面显示 `新建` 按钮，因为我们不需要在后台新建用户
        $grid->disableCreateButton();
        // 同时在每一行也不显示 `编辑` 按钮
        $grid->disableActions();

        $grid->tools(
            function($tools) {
                // 禁用批量删除按钮
                $tools->batch(
                    function($batch) {
                        $batch->disableDelete();
                    }
                );
            }
        );
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('email', __('Email'));
        $show->field('email_verified_at', __('Email verified at'));
        $show->field('password', __('Password'));
        $show->field('remember_token', __('Remember token'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }
}
