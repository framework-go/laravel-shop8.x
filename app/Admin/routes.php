<?php

use Illuminate\Routing\Router;
use Encore\Admin\Facades\Admin;
use Illuminate\Support\Facades\Route;

Admin::routes();

Route::group(
    [
        'prefix'     => config('admin.route.prefix'),
        'namespace'  => config('admin.route.namespace'),
        'middleware' => config('admin.route.middleware'),
    ],
    function(Router $router) {
        # 主页
        $router->get('/', 'HomeController@index')->name('home');
        # 用户
        $router->get('users', [App\Admin\Controllers\UsersController::class, 'index']);

        # 商品
        $router->get('products', [App\Admin\Controllers\ProductsController::class, 'index']);
        $router->get('products/{id}', [App\Admin\Controllers\ProductsController::class, 'show']);
        $router->get('products/create', [App\Admin\Controllers\ProductsController::class, 'create']);
        $router->post('products', [App\Admin\Controllers\ProductsController::class, 'store']);
        $router->get('products/{id}/edit', [App\Admin\Controllers\ProductsController::class, 'edit']);
        $router->put('products/{id}', [App\Admin\Controllers\ProductsController::class, 'update']);
        $router->delete('products/{id}', [App\Admin\Controllers\ProductsController::class, 'destroy']);
    }
);
